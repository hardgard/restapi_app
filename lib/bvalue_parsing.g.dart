// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bvalue_parsing.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BuiltPost extends BuiltPost {
  factory _$BuiltPost([void updates(BuiltPostBuilder b)]) =>
      (new BuiltPostBuilder()..update(updates)).build();

  _$BuiltPost._() : super._();

  @override
  BuiltPost rebuild(void updates(BuiltPostBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  BuiltPostBuilder toBuilder() => new BuiltPostBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BuiltPost;
  }

  @override
  int get hashCode {
    return 168456944;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('BuiltPost').toString();
  }
}

class BuiltPostBuilder implements Builder<BuiltPost, BuiltPostBuilder> {
  _$BuiltPost _$v;

  BuiltPostBuilder();

  @override
  void replace(BuiltPost other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BuiltPost;
  }

  @override
  void update(void updates(BuiltPostBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$BuiltPost build() {
    final _$result = _$v ?? new _$BuiltPost._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
