import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:restapi_app/controller/post.dart';

void main() => runApp(MyApp(post: fetchPost()));

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Future<Post> post;

  MyApp({Key key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: statelessRest());
  }
}

Future<Post> fetchPost() async {
  final response =
      await http.get('https://jsonplaceholder.typicode.com/posts/1');
  if (response.statusCode == 200) {
    return Post.fromJson(json.decode(response.body));
  } else {
    throw Exception("Couldn't load JSON");
  }
}

Widget statelessRest() {
  Future<Post> post;

  return Scaffold(
    appBar: AppBar(
      title: Text("Test Rest"),
    ),
    body: Center(
      child: FutureBuilder<Post>(
        future: post,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  ExpansionTile(title: Text(snapshot.data.title)),
                  IconButton(icon: Icon(Icons.launch), onPressed: () {})
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return CircularProgressIndicator();
        },
      ),
    ),
  );
}
