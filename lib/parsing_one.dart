import 'package:restapi_app/controller/post.dart';
import 'dart:convert' as json;

Post parseOne(String jsonString) {
  final parsed = json.jsonDecode(jsonString);
  Post post = Post.fromJson(parsed);
  return post;
}
