import 'package:built_value/built_value.dart';
part 'bvalue_parsing.g.dart';

abstract class BuiltPost implements Built<BuiltPost, BuiltPostBuilder> {
  BuiltPost._();
  factory BuiltPost([updates(BuiltPostBuilder b)]) = _$BuiltPost;
}
