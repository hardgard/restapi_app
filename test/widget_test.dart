import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:restapi_app/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and triger a frame.
    await tester.pumpWidget(MyApp());
    expect(find.text("Test Rest"), findsOneWidget);
  });
}
