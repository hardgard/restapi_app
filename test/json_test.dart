import 'package:restapi_app/parsing_one.dart';
import 'package:test_api/test_api.dart';
import 'dart:convert' as json;

void main() {
  test("fetching jsonItem", () {
    const jsonString = """ {
  "userId": 1,
  "id": 1,
  "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  "body": "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam ostrum rerum est autem sunt rem eveniet architecto"
}""";
    expect(parseOne(jsonString).title,
        "sunt aut facere repellat provident occaecati excepturi optio reprehenderit");
  });
}
